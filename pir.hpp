#ifndef PIR_HPP_
#define PIR_HPP_

#include "ctxt_heap.hpp"


void binary_equals(Ctxt& result, const CtPtrs& left, const CtPtrs& right) {
	ctxt_heap_product heap;

	size_t min_size = std::min(left.size(), right.size());
	size_t max_size = std::max(left.size(), right.size());

	for (size_t i = 0; i < min_size; i++) {
		Ctxt temp(ZeroCtxtLike, result);
		temp.addConstant(ZZ(1));
		temp.addCtxt(*left[i]);
		temp.addCtxt(*right[i]);
		heap.store(temp);
	}

	for (size_t i = min_size; i < max_size; i++) {
		Ctxt temp(ZeroCtxtLike, result);
		temp.addConstant(ZZ(1));

		if (left.isSet(i)) {
			temp.addCtxt(*left[i]);
		}
		else if (right.isSet(i)){
			temp.addCtxt(*right[i]);
		}

		heap.store(temp);
	}

	if (!heap.is_empty()) {
		heap.extract_result(result);
	}
	else {
		result = Ctxt(ZeroCtxtLike, result);
	}
}


//void compare_and_pack(const EncryptedArray& ea, Ctxt& packed_result, const CtPtrMat& values_vectors, const CtPtrs& query_vector) {
//	assert(ea.size() >= values_vectors.size());
//
//	packed_result = Ctxt(ZeroCtxtLike, packed_result);
//
//	for (size_t i = 0; i < size_t(values_vectors.size()); i++) {
//		Ctxt equals_result(ZeroCtxtLike, packed_result);
//		binary_equals(equals_result, values_vectors[i], query_vector);
//		ZZX mask;
//		ea.encodeUnitSelector(mask, i);
//		equals_result.multByConstant(mask);
//		packed_result.addCtxt(equals_result);
//	}
//}


#endif /* PIR_HPP_ */
