#ifndef BINARY_RAFFLE_HPP_
#define BINARY_RAFFLE_HPP_

#include <functional>
#include <algorithm>

#include <FHE/FHE.h>
#include <FHE/EncryptedArray.h>
#include <NTL/GF2.h>
//#include <FHE/replicate.h>
//#include <FHE/debugging.h>

#include "common.hpp"
#include "ctxt_heap.hpp"
//#include "replicate_handlers.hpp"


class piecewise_binary_raffle {
private:
	const EncryptedArray& _ea;
	const FHEPubKey& _public_key;
	const FHESecKey& _secret_key;

	const size_t _samples_count;

	//intermediate storage
	Ctxt _prev_step_indicator;
	std::vector<Ctxt> _random_prefix_sums;

	std::vector<Ctxt> _output;
	size_t _processed_indices;
public:
	piecewise_binary_raffle(const EncryptedArray& ea, const FHEPubKey& public_key, const FHESecKey& secret_key, size_t samples_count) :
		_ea(ea),
		_public_key(public_key),
		_secret_key(secret_key),
		_samples_count(samples_count),
		_prev_step_indicator(Ctxt(_public_key)),
		_random_prefix_sums(std::vector<Ctxt>(samples_count ,Ctxt(_public_key))),
		_processed_indices(0)
	{	}

	static size_t calc_output_size(size_t input_size) {
		return ciel_log2(input_size + 1);
	}

	static size_t calc_default_samples_count(size_t input_size) {
		return std::pow(ciel_log2(input_size), 2);
	}

	static size_t calc_samples_count(size_t input_size, size_t bit_security) {
		return ciel_log2(input_size) + bit_security;
	}

	static size_t calc_level_approximation(size_t samples_count) {
		return ciel_log2(samples_count) + 4;
	}

	size_t get_processed_indices() const { return _processed_indices; }

	size_t get_output_size() const { return _output.size(); }

	template<class InputIt>
	void process_piece(InputIt first, InputIt last) {
		size_t piece_size = std::distance(first, last);
		for (size_t i = 0; i < piece_size; i++) {
			ctxt_heap_product product_heap;

			for (size_t sample = 0; sample < _samples_count; sample++) {
				if(NTL::IsOne(NTL::random_GF2())) {
					_random_prefix_sums[sample].addCtxt(*(first + i));
				}

				Ctxt product_ctxt(ZeroCtxtLike, *first);
				product_ctxt = _random_prefix_sums[sample];
				product_ctxt.addConstant(ZZ(1));
				product_heap.store(product_ctxt);
			}


			Ctxt step_indicator(ZeroCtxtLike, *first);
			product_heap.extract_result(step_indicator);
			product_heap.clear();
			step_indicator.addConstant(ZZ(1));

			Ctxt pairwise_diff(ZeroCtxtLike, *first);
			pairwise_diff = step_indicator;
			if (i > 0) {
				pairwise_diff.addCtxt(_prev_step_indicator);
			}

			//add output bits if needed
			size_t log2_processed_indices_plus2 = ciel_log2(_processed_indices + 2);
			for (size_t o = _output.size(); o < log2_processed_indices_plus2; o++) {
				_output.emplace_back(Ctxt(_public_key));
			}

			for (size_t bit_i = 0; bit_i < _output.size(); bit_i++) {
				if (bit(_processed_indices + 1, bit_i) == 1) {
					_output[bit_i].addCtxt(pairwise_diff);
				}
			}

			_processed_indices++;

			//store for next iteration
			_prev_step_indicator = step_indicator;
		}
	}

	template<class OutputIt>
	void get_result(OutputIt first, OutputIt last) {
//		if (std::distance(first, last) >= long(_output.size())) {
			std::copy(_output.begin(), _output.end(), first);
//		}
	}


//	const EncryptedArray& get_ea() { return _ea; };
//	const FHEPubKey& get_public_key()  { return _public_key; };
//	const FHESecKey& get_secret_key()  { return _secret_key; };


	~piecewise_binary_raffle() {
		_random_prefix_sums.clear();
		_random_prefix_sums.shrink_to_fit();
	}
};


class parallel_piecewise_binary_raffle {
private:
	uint _threads_count;
	std::vector<piecewise_binary_raffle> _piecewise_binary_raffles;
public:
	parallel_piecewise_binary_raffle(const EncryptedArray& ea, const FHEPubKey& public_key, const FHESecKey& secret_key,size_t samples_count, uint threads_count = 0) :
		_threads_count(threads_count) {
		if (_threads_count == 0) {
			_threads_count  = get_hardware_thread_count();
		}

		_piecewise_binary_raffles = std::vector<piecewise_binary_raffle>(_threads_count,
				piecewise_binary_raffle(ea, public_key, secret_key, samples_count));
	}

	template<class InputIt>
	void process_piece(InputIt first, InputIt last) {
		size_t n = std::distance(first, last);
		size_t block_size = n / _threads_count;

		std::vector<std::thread> threads(_threads_count - 1);

		InputIt block_start = first;
		for(size_t t = 0; t < _threads_count - 1; t++) {
			InputIt block_end = block_start;
			std::advance(block_end, block_size);

			threads[t] = std::thread([this, t] (InputIt block_first, InputIt block_last) {
				_piecewise_binary_raffles[t].process_piece(block_first, block_last);
			}, block_start, block_end);

			std::advance(block_start, block_size);
		}

		_piecewise_binary_raffles[_threads_count - 1].process_piece(block_start, last);

		std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
	}


	template<class OutputIt>
	void get_results(std::vector<OutputIt> first_iterators, std::vector<OutputIt> last_iterators) {
		std::vector<std::thread> threads(_threads_count - 1);

		for (size_t t = 0; t <_piecewise_binary_raffles.size() - 1; t++) {
//			if (first_iterators.size() >= t && last_iterators.size() >= t) {
			threads[t] = std::thread([this, t] (OutputIt first_iterator, OutputIt last_iterator) {
				_piecewise_binary_raffles[t].get_result(first_iterator, last_iterator);
			}, first_iterators[t], last_iterators[t]);
//			}
		}

		_piecewise_binary_raffles[_threads_count - 1].get_result(
				first_iterators[_threads_count - 1], last_iterators[_threads_count - 1]);

		std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
	}
};


template<class InputIt>
void compare(Ctxt& output, const InputIt left, const InputIt right, size_t size) {
//	assert(left.size() == right.size());
	ctxt_heap_product product_heap;

	for (size_t i = 0; i < size; i++) {
		Ctxt tmp(ZeroCtxtLike, output);
		tmp.addConstant(ZZ(1));
		tmp.addCtxt(*(left + i));
		tmp.addCtxt(*(right + i));
		product_heap.store(tmp);
	}

	product_heap.extract_result(output);
}


template<class InputIt>
void greater_than(Ctxt& output, const InputIt left, const InputIt right, size_t size) {
//	assert(left.size() == right.size());
	ctxt_heap_product product_heap;
	output.clear();

//	long left_val = 0, right_val = 0;
//	cout << "left:" << endl;
//	for (size_t i = 0; i < size; i++) {
//		vector<long> ptxt;
//		ea.decrypt(*(left + i), secret_key, ptxt);
//		left_val += ptxt[0] * std::pow(2, i);
//		cout << "["<< i << " = " << ptxt[0] << "]";
//	}
//	cout << endl << left_val << endl;

//	cout << "right:" << endl;
//	for (size_t i = 0; i < size; i++) {
//		vector<long> ptxt;
//		ea.decrypt(*(right + i), secret_key, ptxt);
//		right_val += ptxt[0] * std::pow(2, i);
//		cout << "["<< i << " = " << ptxt[0] << "]";
//	}
//
//	cout << endl << right_val << endl;


	for (size_t i = 0; i < size; i++) {
		InputIt cur_left = left;
		std::advance(cur_left, size - i - 1);
		InputIt cur_right = right;
		std::advance(cur_right, size - i - 1);

//		{
//			vector<long> ptxt;
//			ea.decrypt(*cur_left, secret_key, ptxt);
//			cout << "i = " << i << ": cur_left = " << ptxt[0] << endl;
//		}
//
//		{
//			vector<long> ptxt;
//			ea.decrypt(*cur_right, secret_key, ptxt);
//			cout << "i = " << i << ": cur_right = " << ptxt[0] << endl;
//		}

		// left[end - i] > right[end - i]
		// left[end - i] * ( right[end - i] + 1 )
		Ctxt gt(ZeroCtxtLike, output);
		gt.addCtxt(*cur_right);
		gt.addConstant(ZZ(1));
		gt.multiplyBy(*cur_left);

//		{
//			vector<long> ptxt;
//			ea.decrypt(gt, secret_key, ptxt);
//			cout << "i = " << i << ": gt = " << ptxt[0] << endl;
//		}

		if (i > 0) {
			Ctxt total_product(ZeroCtxtLike, output);
			product_heap.extract_result(total_product);
			total_product.multiplyBy(gt);

//			{
//				vector<long> ptxt;
//				ea.decrypt(total_product, secret_key, ptxt);
//				cout << "i = " << i << ": total_product = " << ptxt[0] << endl;
//			}

			output.addCtxt(total_product);
		}
		else {
			output.addCtxt(gt);
		}

//		{
//			vector<long> ptxt;
//			ea.decrypt(output, secret_key, ptxt);
//			cout << "i = " << i << ": output = " << ptxt[0] << endl;
//		}

		// left[end - i] == right[end - i]
		// left[end - i] + right[end - i] + 1
		Ctxt eq(ZeroCtxtLike, output);
		eq.addCtxt(*cur_left);
		eq.addCtxt(*cur_right);
		eq.addConstant(ZZ(1));

//		{
//			vector<long> ptxt;
//			ea.decrypt(eq, secret_key, ptxt);
//			cout << "i = " << i << ": eq = " << ptxt[0] << endl;
//		}

		product_heap.store(eq);
	}

	product_heap.clear();
}

template<class QueryInputIt>
class parallel_piecewise_secure_search {
private:
	uint _threads_count;
	std::vector<piecewise_binary_raffle> _piecewise_binary_raffles;
	QueryInputIt _query_first;
	size_t _query_size;
public:
	parallel_piecewise_secure_search(const EncryptedArray& ea, const FHEPubKey& public_key, const FHESecKey& secret_key,
		QueryInputIt query_first, size_t query_size, size_t samples_count, uint threads_count = 0) :
		_threads_count(threads_count), _query_first(query_first), _query_size(query_size) {
		if (_threads_count == 0) {
			_threads_count  = get_hardware_thread_count();
		}

		_piecewise_binary_raffles = std::vector<piecewise_binary_raffle>(_threads_count,
				piecewise_binary_raffle(ea, public_key, secret_key, samples_count));
	}



	template<class InputIt>
	void process_piece(InputIt first, InputIt last) {
		size_t n = std::distance(first, last);
		size_t block_size = n / _threads_count;

		std::vector<std::thread> threads(_threads_count - 1);

		InputIt block_start = first;
		for(size_t t = 0; t < _threads_count - 1; t++) {
			InputIt block_end = block_start;
			std::advance(block_end, block_size);

			threads[t] = std::thread([this, t] (InputIt block_first, InputIt block_last) {
				std::vector<Ctxt> cmp_result(std::distance(block_first, block_last), Ctxt(ZeroCtxtLike, *(block_first[0])));

				InputIt current = block_first;
				for (size_t i = 0; current != block_last; i++) {
					compare(cmp_result[i], *current, this->_query_first, this->_query_size);

					++current;
				}

				_piecewise_binary_raffles[t].process_piece(cmp_result.begin(), cmp_result.end());

			}, block_start, block_end);

			std::advance(block_start, block_size);
		}

		std::vector<Ctxt> cmp_result(std::distance(block_start, last), Ctxt(ZeroCtxtLike, *(block_start[0])));

		InputIt current = block_start;

		for (size_t i = 0; current != last; i++) {
			compare(cmp_result[i], *current, this->_query_first, this->_query_size);
			++current;
		}

		_piecewise_binary_raffles[_threads_count - 1].process_piece(cmp_result.begin(), cmp_result.end());


		std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
	}


	template<class OutputIt>
	void get_results(std::vector<OutputIt> first_iterators, std::vector<OutputIt> last_iterators) {
		std::vector<std::thread> threads(_threads_count - 1);

		for (size_t t = 0; t <_piecewise_binary_raffles.size() - 1; t++) {
//			if (first_iterators.size() >= t && last_iterators.size() >= t) {
			threads[t] = std::thread([this, t] (OutputIt first_iterator, OutputIt last_iterator) {
				_piecewise_binary_raffles[t].get_result(first_iterator, last_iterator);
			}, first_iterators[t], last_iterators[t]);
//			}
		}

		_piecewise_binary_raffles[_threads_count - 1].get_result(
				first_iterators[_threads_count - 1], last_iterators[_threads_count - 1]);

		std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
	}
};




template<class QueryInputIt>
class parallel_piecewise_secure_search_range_query {
private:
	uint _threads_count;
	std::vector<piecewise_binary_raffle> _piecewise_binary_raffles;
	QueryInputIt _query_low;
	QueryInputIt _query_high;
	size_t _query_size;
public:
	parallel_piecewise_secure_search_range_query(const EncryptedArray& ea, const FHEPubKey& public_key, const FHESecKey& secret_key,
		QueryInputIt query_low, QueryInputIt query_high, size_t query_size, size_t samples_count, uint threads_count = 0) :
		_threads_count(threads_count), _query_low(query_low),_query_high(query_high), _query_size(query_size) {
		if (_threads_count == 0) {
			_threads_count  = get_hardware_thread_count();
		}

		_piecewise_binary_raffles = std::vector<piecewise_binary_raffle>(_threads_count,
				piecewise_binary_raffle(ea, public_key, secret_key, samples_count));
	}



	template<class InputIt>
	void process_piece(InputIt first, InputIt last) {
		size_t n = std::distance(first, last);
		size_t block_size = n / _threads_count;

		std::vector<std::thread> threads(_threads_count - 1);

		InputIt block_start = first;
		for(size_t t = 0; t < _threads_count - 1; t++) {
			InputIt block_end = block_start;
			std::advance(block_end, block_size);

			threads[t] = std::thread([this, t] (InputIt block_first, InputIt block_last) {
				std::vector<Ctxt> gt_result1(std::distance(block_first, block_last), Ctxt(ZeroCtxtLike, *(block_first[0])));
				std::vector<Ctxt> gt_result2(std::distance(block_first, block_last), Ctxt(ZeroCtxtLike, *(block_first[0])));
				InputIt current = block_first;
				for (size_t i = 0; current != block_last; i++) {

					greater_than(gt_result1[i], *current, this->_query_low, this->_query_size);
					greater_than(gt_result2[i], this->_query_high, *current, this->_query_size);
					gt_result1[i].multiplyBy(gt_result2[i]);
					++current;
				}

				_piecewise_binary_raffles[t].process_piece(gt_result1.begin(), gt_result1.end());

			}, block_start, block_end);

			std::advance(block_start, block_size);
		}

		std::vector<Ctxt> gt_result1(std::distance(block_start, last), Ctxt(ZeroCtxtLike, *(block_start[0])));
		std::vector<Ctxt> gt_result2(std::distance(block_start, last), Ctxt(ZeroCtxtLike, *(block_start[0])));
		InputIt current = block_start;

		for (size_t i = 0; current != last; i++) {
			greater_than(gt_result1[i], *current, this->_query_low, this->_query_size);
			greater_than(gt_result2[i], this->_query_high, *current, this->_query_size);
			gt_result1[i].multiplyBy(gt_result2[i]);

//			{
//				vector<long> ptxt;
//				_piecewise_binary_raffles[_threads_count - 1].get_ea().decrypt(gt_result1[i], _piecewise_binary_raffles[_threads_count - 1].get_secret_key(), ptxt);
//				cout << "i = " << i << ": output = " << ptxt[0] << endl;
//			}


			++current;
		}

		_piecewise_binary_raffles[_threads_count - 1].process_piece(gt_result1.begin(), gt_result1.end());


		std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
	}


	template<class OutputIt>
	void get_results(std::vector<OutputIt> first_iterators, std::vector<OutputIt> last_iterators) {
		std::vector<std::thread> threads(_threads_count - 1);

		for (size_t t = 0; t <_piecewise_binary_raffles.size() - 1; t++) {
//			if (first_iterators.size() >= t && last_iterators.size() >= t) {
			threads[t] = std::thread([this, t] (OutputIt first_iterator, OutputIt last_iterator) {
				_piecewise_binary_raffles[t].get_result(first_iterator, last_iterator);
			}, first_iterators[t], last_iterators[t]);
//			}
		}

		_piecewise_binary_raffles[_threads_count - 1].get_result(
				first_iterators[_threads_count - 1], last_iterators[_threads_count - 1]);

		std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
	}
};


#endif /* BINARY_RAFFLE_HPP_ */
