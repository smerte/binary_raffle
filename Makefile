DEBUG_FLAGS =	-g -O0 -std=c++17 -DPRINT_DEBUG_MODE -fmessage-length=0
RELEASE_FLAGS =	-g -O3 -std=c++17 -fmessage-length=0

LIBS = -lbenchmark -lfhe -lntl -lgmp -lpthread
TIME_NOW := $(shell /bin/date "+%Y_%m_%d_%H_%M_%S")

all:	release

release:
	mkdir -p ./release_$(TIME_NOW)
	$(CXX) $(RELEASE_FLAGS) -c -o ./release_$(TIME_NOW)/benchmark.o benchmark.cpp
	$(CXX) -o ./release_$(TIME_NOW)/benchmark ./release_$(TIME_NOW)/benchmark.o $(LIBS)

debug:
	mkdir -p ./debug_$(TIME_NOW)
	$(CXX) $(DEBUG_FLAGS) -c -o ./debug_$(TIME_NOW)/benchmark.o benchmark.cpp
	$(CXX) -o ./debug_$(TIME_NOW)/benchmark ./debug_$(TIME_NOW)/benchmark.o $(LIBS)
	
clean_release:
	rm -rf ./release_*
	
clean_debug:
	rm -rf ./debug_*
	
clean:	clean_debug	clean_release