#ifndef BENCHMARK_BM_BINARY_RAFFLE_HPP_
#define BENCHMARK_BM_BINARY_RAFFLE_HPP_

#include <benchmark/benchmark.h>

#include "../ad_hoc_ctxt_iterator.hpp"
#include "../binary_raffle.hpp"
#include "../helib_context.hpp"
#include "../common.hpp"

static void args_binary_raffle(benchmark::internal::Benchmark* b) {
	for (int input_bit_size = 3; input_bit_size <= 11; ++input_bit_size) {
		b->Args({input_bit_size, 1});
		b->Args({input_bit_size, 10});
		b->Args({input_bit_size, 20});
		b->Args({input_bit_size, 40});
		b->Args({input_bit_size, 80});

//		size_t input_size = std::pow(2, input_bit_size);

//		b->Args({input_bit_size, 1});
//
//		int samples_count = ciel_log2(input_size);
//		b->Args({input_bit_size, samples_count});

//		samples_count = piecewise_binary_raffle::calc_default_samples_count(input_size);
		 //, samples_count});
	}
}


static void bm_piecewise_binary_raffle_parallel(benchmark::State& state) {
	//initilize
	size_t single_thread_input_size = std::pow(2, state.range(0));
	size_t total_input_size = single_thread_input_size * get_hardware_thread_count();
//	size_t samples_count = piecewise_binary_raffle::calc_default_samples_count(single_thread_input_size);

	int bit_security = state.range(1);
	double failure_probability = 1L / (std::pow(2L, bit_security));
	size_t samples_count = piecewise_binary_raffle::calc_samples_count(single_thread_input_size, bit_security);
	size_t level = piecewise_binary_raffle::calc_level_approximation(samples_count);
	helib_context context(false, level, 2, 1, 80, 3, 1, 128, 500);

	size_t thread_count = get_hardware_thread_count();

	parallel_piecewise_binary_raffle br(*context.encrypted_array, *context.public_key, *context.secret_key, samples_count);

	//prepare input
	std::vector<NewPlaintextArray> plaintext;
	plaintext.reserve(total_input_size);
	for (size_t i = 0; i < total_input_size; i++) {
		NewPlaintextArray plain(*context.encrypted_array);
		if (i <  total_input_size / 2) {
			encode(*context.encrypted_array, plain, 0);
		}
		else {
			long random_bit = rep(random_GF2());
			encode(*context.encrypted_array, plain, random_bit);
		}

		plaintext.push_back(plain);
	}

//	//print input
//	for (size_t i = 0; i < total_input_size; i++) {
//		cout << i << "\t" << plaintext[i] << endl;
//	}

	//find actual first positive index
	size_t actual_first_positive_index = 0;
	NewPlaintextArray one_bit(*context.encrypted_array);
	encode(*context.encrypted_array, one_bit, 1);
	for (size_t i = 0; i < total_input_size; i++) {
		if (equals(*context.encrypted_array, plaintext[i], one_bit)) {
			actual_first_positive_index = i;
			break;
		}
	}

	size_t found_first_positive_index = 0;
	bool all_ctxts_correct = true;
	for (auto _ : state) {
		state.PauseTiming();
		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>
			enc_it_begin(*context.public_key, *context.encrypted_array, plaintext.begin());
		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>
			enc_it_end(*context.public_key, *context.encrypted_array, plaintext.end());

		size_t single_thread_output_size = piecewise_binary_raffle::calc_output_size(single_thread_input_size);
		std::vector<std::vector<Ctxt>> output_vectors(thread_count, std::vector<Ctxt>(single_thread_output_size, Ctxt(*context.public_key)));
		std::vector<std::vector<Ctxt>::iterator> first_iterators;
		first_iterators.reserve(output_vectors.size());
		std::vector<std::vector<Ctxt>::iterator> last_iterators;
		last_iterators.reserve(output_vectors.size());
		for (size_t i = 0; i < output_vectors.size(); i++) {
			first_iterators.push_back(output_vectors[i].begin());
			last_iterators.push_back(output_vectors[i].end());
		}

		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>::reset_elapsed_time();
		state.ResumeTiming();
		br.process_piece(enc_it_begin, enc_it_end);
		br.get_results(first_iterators, last_iterators);
		state.PauseTiming();

		for (size_t t = 0; t < thread_count; t++) {
			size_t local_found_first_positive_index = 0;
			for (size_t i = 0; i < output_vectors[t].size(); i++) {
				vector<long> ptxt;
				context.encrypted_array->decrypt(output_vectors[t][i], *context.secret_key, ptxt);
//				cout << i << " " << ptxt[0] << endl;
				local_found_first_positive_index += (ptxt[0] * std::pow(2, i));

				all_ctxts_correct = all_ctxts_correct && output_vectors[t][i].isCorrect();
			}

			if (found_first_positive_index == 0 && local_found_first_positive_index != 0) {
				found_first_positive_index = (t * single_thread_input_size) + local_found_first_positive_index - 1;
			}
		}
	}

	state.counters.insert({
		{"all_ctxts_correct", all_ctxts_correct},
		{"is_correct", (found_first_positive_index == actual_first_positive_index)},
		{"total_input_size", total_input_size},
		{"samples_count", samples_count},
		{"bit_security", bit_security},
		{"failure_probability", failure_probability},
		{"thread_count", thread_count},
		{"helib_level", level},
		{"helib_simd_size", context.encrypted_array->size()},
		{"helib_m", context.param_m},
		{"ad_hoc_enc_ns", ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>::get_elapsed_time()}
	});
}

//BENCHMARK(bm_piecewise_binary_raffle_parallel)->Apply(args_binary_raffle)->MinTime(1e-12)->Repetitions(1);

#endif /* BENCHMARK_BM_BINARY_RAFFLE_HPP_ */
