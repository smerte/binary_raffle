#ifndef BENCHMARK_BM_SPIRIT_HPP_
#define BENCHMARK_BM_SPIRIT_HPP_


#include <benchmark/benchmark.h>
#include "../ad_hoc_ctxt_iterator.hpp"
#include "../spirit.hpp"
#include "../helib_context.hpp"
#include "../common.hpp"
#include <limits>


static void args_spirit(benchmark::internal::Benchmark* b) {
	for (int input_bit_size = 3; input_bit_size <= 32; ++input_bit_size) {
//		long input_size = std::pow(2, input_bit_size); // n
		int prime_bound = input_bit_size;  //log(n)
		int prime_group_size = std::ceil((input_bit_size * input_bit_size) / ciel_log2(input_bit_size) ); //log^2(n)/loglog(n)
		NTL::PrimeSeq s;
		int prime = s.next();
		int smaller_prime_count = 0, prime_count = 0;
		while (prime && prime_count < prime_group_size /*&& prime < input_size*/) {
			if (prime > prime_bound) {
				if (smaller_prime_count >= prime_group_size) {
					b->Args({input_bit_size, prime});
					prime_count++;
				}
				
				smaller_prime_count++;
			}

			prime = s.next();
		}
	}
}

long calc_additional_levels(size_t log_input_size, size_t prime) {
	size_t input_size = std::pow(2, log_input_size);
	long levels = spirit::calc_level_approximation(log_input_size, prime);
	long additional_levels = 0;

	bool is_correct = true;
	do {
		helib_context context(false, levels + additional_levels, prime, 1, 80, 3, 1, 128, 500);

		std::vector<NewPlaintextArray> plaintext;
		plaintext.reserve(input_size);
		for (size_t i = 0; i < input_size; i++) {
			NewPlaintextArray plain(*context.encrypted_array);
			long random_bit = rep(random_GF2());
			encode(*context.encrypted_array, plain, random_bit);
			plaintext.push_back(plain);
		}

		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>
					enc_it_begin(*context.public_key, *context.encrypted_array, plaintext.begin());
		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>
			enc_it_end(*context.public_key, *context.encrypted_array, plaintext.end());

		std::vector<Ctxt> output = std::vector<Ctxt>(ciel_log2(input_size), Ctxt(*context.public_key));

		spirit sp(*context.encrypted_array, *context.public_key, *context.secret_key);
		sp.execute(enc_it_begin, enc_it_end, output.begin());

		is_correct = true;
		for (size_t i = 0; i < output.size(); i++) {
			is_correct = is_correct && output[i].isCorrect();
		}

		if (!is_correct) {
			additional_levels += 3;
		}
	} while (!is_correct);

	return additional_levels;
}


static void bm_parallel_spirit(benchmark::State& state) {
	size_t thread_count = get_hardware_thread_count();
	long single_thread_input_bit_size = state.range(0);
	size_t single_thread_input_size = std::pow(2, single_thread_input_bit_size);
	size_t total_input_size = single_thread_input_size * thread_count;
	long prime = state.range(1);

	long levels = spirit::calc_level_approximation(single_thread_input_bit_size, prime);

	long additional_levels = calc_additional_levels(single_thread_input_bit_size, prime);

	helib_context context(false, levels + additional_levels, prime, 1, 80, 3, 1, 128, 500);
	parallel_spirit sp(*context.encrypted_array, *context.public_key, *context.secret_key);

	std::vector<NewPlaintextArray> plaintext;
	plaintext.reserve(total_input_size);
	for (size_t i = 0; i < total_input_size; i++) {
		NewPlaintextArray plain(*context.encrypted_array);
		long random_bit = rep(random_GF2());
		encode(*context.encrypted_array, plain, random_bit);
		plaintext.push_back(plain);
	}


//	//print input
//	for (size_t i = 0; i < plaintext.size(); i++) {
//
//		cout << "index: "<< i << "\t thread: " << (i / single_thread_input_size) << "\t thread_index: " << (i % single_thread_input_size) << "\t value: " << plaintext[i] << endl;
//	}


	size_t output_remaining_min_level = std::numeric_limits<size_t>::max();
	for (auto _ : state) {
		state.PauseTiming();
		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>
			enc_it_begin(*context.public_key, *context.encrypted_array, plaintext.begin());
		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>
			enc_it_end(*context.public_key, *context.encrypted_array, plaintext.end());

		size_t single_thread_output_size = spirit::calc_output_size(single_thread_input_size);
		std::vector<std::vector<Ctxt>> output_vectors(thread_count, std::vector<Ctxt>(single_thread_output_size, Ctxt(*context.public_key)));
		std::vector<std::vector<Ctxt>::iterator> output_iterators;
		output_iterators.reserve(output_vectors.size());

		for (size_t i = 0; i < output_vectors.size(); i++) {
			output_iterators.push_back(output_vectors[i].begin());
		}

		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>::reset_elapsed_time();
		state.ResumeTiming();
		sp.execute(enc_it_begin, enc_it_end, output_iterators);
		state.PauseTiming();

		for (size_t t = 0; t < thread_count; t++) {
//			size_t local_found_first_positive_index = 0;
			for (size_t i = 0; i < output_vectors[t].size(); i++) {
				output_remaining_min_level = std::min(output_remaining_min_level, size_t(output_vectors[t][i].findBaseLevel()));

//				vector<long> ptxt;
//				context.encrypted_array->decrypt(output_vectors[t][i], *context.secret_key, ptxt);
//				local_found_first_positive_index += (ptxt[0] * std::pow(2, i));
			}

//			cout << "thread: " << t << "\t local_found_first_positive_index: " << local_found_first_positive_index << endl;
		}
	}

	state.counters.insert({
		{"single_thread_input_size", single_thread_input_size},
		{"thread_count", thread_count},
		{"total_input_size", total_input_size},
		{"prime", prime},
		{"helib_levels_initial", levels},
		{"helib_levels_additional", additional_levels},
		{"helib_levels_total", levels + additional_levels},
		{"helib_simd_size", context.encrypted_array->size()},
		{"helib_m", context.param_m},
		{"output_remaining_min_level", output_remaining_min_level},
		{"ad_hoc_enc_ns", ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>::get_elapsed_time()}
	});
}

static void bm_spirit(benchmark::State& state) {
	size_t log_input_size = state.range(0);
	size_t input_size = std::pow(2, log_input_size); //n
	long prime = state.range(1);
	long levels = spirit::calc_level_approximation(log_input_size, prime);

	long additional_levels = calc_additional_levels(log_input_size, prime);

	helib_context context(false, levels + additional_levels, prime, 1, 80, 3, 1, 128, 500);

	std::vector<NewPlaintextArray> plaintext;
	plaintext.reserve(input_size);
	for (size_t i = 0; i < input_size; i++) {
		NewPlaintextArray plain(*context.encrypted_array);
		if (i <  input_size / 2) {
			encode(*context.encrypted_array, plain, 0);
		}
		else {
			long random_bit = rep(random_GF2());
			encode(*context.encrypted_array, plain, random_bit);
		}

		plaintext.push_back(plain);
	}


	//find actual first positive index
	size_t actual_first_positive_index = 0;
	NewPlaintextArray one_bit(*context.encrypted_array);
	encode(*context.encrypted_array, one_bit, 1);
	for (size_t i = 0; i < input_size; i++) {
		if (equals(*context.encrypted_array, plaintext[i], one_bit)) {
			actual_first_positive_index = i;
			break;
		}
	}

//	//print input
//	for (size_t i = 0; i < plaintext.size(); i++) {
//		cout << i << "\t" << plaintext[i] << endl;
//	}

	spirit sp(*context.encrypted_array, *context.public_key, *context.secret_key);

	size_t found_first_positive_index = 0, output_remaining_min_level = std::numeric_limits<size_t>::max();
	for (auto _ : state) {
		state.PauseTiming();
		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>
			enc_it_begin(*context.public_key, *context.encrypted_array, plaintext.begin());
		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>
			enc_it_end(*context.public_key, *context.encrypted_array, plaintext.end());


		std::vector<Ctxt> output = std::vector<Ctxt>(ciel_log2(input_size), Ctxt(*context.public_key));

		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>::reset_elapsed_time();
		state.ResumeTiming();
		sp.execute(enc_it_begin, enc_it_end, output.begin());
		state.PauseTiming();

		found_first_positive_index = 0;
		for (size_t i = 0; i < output.size(); i++) {
			vector<long> ptxt;
			output_remaining_min_level = std::min(output_remaining_min_level, size_t(output[i].findBaseLevel()));
			context.encrypted_array->decrypt(output[i], *context.secret_key, ptxt);
			found_first_positive_index += (ptxt[0] * std::pow(2, i));
		}
	}

	state.counters.insert({
		{"input_size", input_size},
		{"is_correct", (found_first_positive_index == actual_first_positive_index)},
		{"prime", prime},
		{"output_remaining_min_level", output_remaining_min_level},
		{"helib_levels_initial", levels},
		{"helib_levels_additional", additional_levels},
		{"helib_levels_total", levels + additional_levels},
		{"helib_simd_size", context.encrypted_array->size()},
		{"helib_m", context.param_m},
		{"ad_hoc_enc_ns", ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>::get_elapsed_time()}
	});
}

BENCHMARK(bm_parallel_spirit)->Apply(args_spirit)->MinTime(1e-12)->Repetitions(1);



////////////////////////////////////////////////////////////////////////////////

static void args_levels(benchmark::internal::Benchmark* b) {
	NTL::PrimeSeq s;
	int prime = s.next();
	while (prime && prime < 10000) {
		for (int l = 10; l <= 30; l += 10) {
			if (prime > 30) {
				b->Args({l, prime});
			}
		}

		prime = s.next();
	}
}

static void bm_levels(benchmark::State& state) {
	long level = state.range(0);
	long prime = state.range(1);

	long fresh_base_level, after_mult_base_level, helib_m, helib_slots;
	for (auto _ : state) {
		helib_context context(false, level, prime, 1, 80, 3, 1, 128, 500);
		helib_m = context.param_m;
		helib_slots = context.param_s;

		Ctxt tmp(*context.public_key);
		NewPlaintextArray plain(*context.encrypted_array);
		encode(*context.encrypted_array, plain, 1);
		context.encrypted_array->encrypt(tmp, *context.public_key, plain);
		fresh_base_level = tmp.findBaseLevel();


		Ctxt tmp2(*context.public_key);
		NewPlaintextArray plain2(*context.encrypted_array);
		encode(*context.encrypted_array, plain2, 1);
		context.encrypted_array->encrypt(tmp2, *context.public_key, plain);

		tmp.multiplyBy(tmp2);
		after_mult_base_level = tmp.findBaseLevel();
	}

	state.counters.insert({
		{"helib_level", level},
		{"helib_prime", prime},
		{"helib_m", helib_m},
		{"helib_slots", helib_slots},
		{"fresh_base_level", fresh_base_level},
		{"after_mult_base_level", after_mult_base_level},
		{"levels_per_mult", fresh_base_level - after_mult_base_level}
	});
}

#endif /* BENCHMARK_BM_SPIRIT_HPP_ */
