#ifndef BENCHMARK_BM_SPIRIT_WITH_EQ_CMP_HPP_
#define BENCHMARK_BM_SPIRIT_WITH_EQ_CMP_HPP_


#include <benchmark/benchmark.h>
#include "../ad_hoc_ctxt_iterator.hpp"
#include "../spirit.hpp"
#include "../helib_context.hpp"
#include "../common.hpp"
#include <limits>


static void args_spirit_with_eq_cmp(benchmark::internal::Benchmark* b) {
	for (int input_bit_size = 6; input_bit_size <= 6; ++input_bit_size) {
//		long input_size = std::pow(2, input_bit_size); // n
		int prime_bound = input_bit_size;  //log(n)
		int prime_group_size = 2 * (std::ceil((input_bit_size * input_bit_size) / std::log2(input_bit_size))) + 1; //2*[log^2(n)/loglog(n)]+1
		int log_value_bit_size = 4;

		NTL::PrimeSeq s;
		int prime = s.next();
		int prime_count = 0;
		while (prime && prime_count < prime_group_size) {
			if (prime > prime_bound) {
				b->Args({input_bit_size, prime, log_value_bit_size});
				prime_count++;
			}

			prime = s.next();
		}
	}
}

long calc_additional_levels_with_eq_cmp(long log_input_size, long prime, long value_bit_size) {
	size_t input_size = std::pow(2, log_input_size);
	long levels = spirit::calc_level_approximation(log_input_size, prime);
	long additional_levels = 0;

	long cmp_levels = ciel_log2(value_bit_size * 2);

	bool is_correct = true;
	do {
		helib_context context(false, levels + additional_levels + cmp_levels, prime, 1, 80, 3, 1, 128, 500);

		//prepare input
		std::vector<std::vector<NewPlaintextArray>> plaintext(input_size, std::vector<NewPlaintextArray>(value_bit_size, NewPlaintextArray(*context.encrypted_array)));

		for (size_t i = 0; i < input_size; i++) {
			for (size_t j = 0; j < size_t(value_bit_size); j++) {
				long random_bit = rep(random_GF2());
				encode(*context.encrypted_array, plaintext[i][j], random_bit);
			}
		}

		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>
		query_it_begin(*context.public_key, *context.encrypted_array, plaintext[3].begin());

		std::vector<Ctxt> cmp_result(input_size, Ctxt(*context.public_key));


		for (size_t i = 0; i < input_size; i++) {
			ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>
			plaintext_it_begin(*context.public_key, *context.encrypted_array, plaintext[i].begin());
			compare_with_squaring(cmp_result[i], plaintext_it_begin, query_it_begin, value_bit_size);
		}

		std::vector<Ctxt> output = std::vector<Ctxt>(ciel_log2(input_size), Ctxt(*context.public_key));

		spirit sp(*context.encrypted_array, *context.public_key, *context.secret_key);
		sp.execute(cmp_result.begin(), cmp_result.end(), output.begin());




		is_correct = true;
		for (size_t i = 0; i < output.size(); i++) {
			is_correct = is_correct && output[i].isCorrect();
		}

//		size_t local_found_first_positive_index = 0;
//		for (size_t i = 0; i < output.size(); i++) {
//			vector<long> ptxt;
//			context.encrypted_array->decrypt(output[i], *context.secret_key, ptxt);
////				cout << i << " " << ptxt[0] << endl;
//			local_found_first_positive_index += (ptxt[0] * std::pow(2, i));
//		}


		if (!is_correct) {
			additional_levels += 3;
		}
	} while (!is_correct);

	return additional_levels;
}


static void bm_parallel_spirit_with_eq_cmp(benchmark::State& state) {
	size_t thread_count = get_hardware_thread_count();
	long single_thread_input_bit_size = state.range(0);
	size_t single_thread_input_size = std::pow(2, single_thread_input_bit_size);
	size_t total_input_size = single_thread_input_size * thread_count;
	long prime = state.range(1);

	int log_value_bit_size = state.range(2);
	int value_bit_size = std::pow(2, log_value_bit_size);

	long cmp_levels = ciel_log2(value_bit_size * 2);

	long levels = spirit::calc_level_approximation(single_thread_input_bit_size, prime);

	long additional_levels = calc_additional_levels_with_eq_cmp(single_thread_input_bit_size, prime, value_bit_size);

	helib_context context(false, levels + additional_levels + cmp_levels, prime, 1, 80, 3, 1, 128, 500);


	std::vector<std::vector<NewPlaintextArray>> plaintext(total_input_size, std::vector<NewPlaintextArray>(value_bit_size, NewPlaintextArray(*context.encrypted_array)));
	for (size_t i = 0; i < total_input_size; i++) {
		for (size_t j = 0; j < size_t(value_bit_size); j++) {
				long random_bit = rep(random_GF2());
				encode(*context.encrypted_array, plaintext[i][j], random_bit);
		}
	}

	ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>
	query_it_begin(*context.public_key, *context.encrypted_array, plaintext[3].begin());

	parallel_piecewise_spirit_secure_search<ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>> sp(*context.encrypted_array, *context.public_key, *context.secret_key,
			query_it_begin, value_bit_size);


//	//print input
//	for (size_t i = 0; i < plaintext.size(); i++) {
//
//		cout << "index: "<< i << "\t thread: " << (i / single_thread_input_size) << "\t thread_index: " << (i % single_thread_input_size) << "\t value: " << plaintext[i] << endl;
//	}


	size_t output_remaining_min_level = std::numeric_limits<size_t>::max();
	for (auto _ : state) {
		state.PauseTiming();
		std::vector<ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>> enc_it_begin_vec;
//		std::vector<ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>> enc_it_end_vec;
		for (size_t i = 0; i < plaintext.size(); i++) {
			enc_it_begin_vec.emplace_back(ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>(
				*context.public_key, *context.encrypted_array, plaintext[i].begin()));
//			enc_it_end_vec.emplace_back(ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>(
//				*context.public_key, *context.encrypted_array, plaintext[i].end()));
		}


		size_t single_thread_output_size = spirit::calc_output_size(single_thread_input_size);


		std::vector<std::vector<Ctxt>> output_vectors(thread_count, std::vector<Ctxt>(single_thread_output_size, Ctxt(*context.public_key)));
		std::vector<std::vector<Ctxt>::iterator> output_iterators;
		output_iterators.reserve(output_vectors.size());

		for (size_t i = 0; i < output_vectors.size(); i++) {
			output_iterators.push_back(output_vectors[i].begin());
		}

		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>::reset_elapsed_time();
		state.ResumeTiming();
		sp.execute(enc_it_begin_vec.begin(), enc_it_begin_vec.end(), output_iterators);
		state.PauseTiming();

		for (size_t t = 0; t < thread_count; t++) {
//			size_t local_found_first_positive_index = 0;
			for (size_t i = 0; i < output_vectors[t].size(); i++) {
				output_remaining_min_level = std::min(output_remaining_min_level, size_t(output_vectors[t][i].findBaseLevel()));

//				vector<long> ptxt;
//				context.encrypted_array->decrypt(output_vectors[t][i], *context.secret_key, ptxt);
//				local_found_first_positive_index += (ptxt[0] * std::pow(2, i));
			}

//			cout << "thread: " << t << "\t local_found_first_positive_index: " << local_found_first_positive_index << endl;
		}
	}

	state.counters.insert({
		{"single_thread_input_size", single_thread_input_size},
		{"thread_count", thread_count},
		{"total_input_size", total_input_size},
		{"prime", prime},
		{"value_bit_size", value_bit_size},
		{"helib_levels_initial", levels},
		{"helib_levels_additional", additional_levels},
		{"helib_levels_total", levels + additional_levels},
		{"helib_simd_size", context.encrypted_array->size()},
		{"helib_m", context.param_m},
		{"output_remaining_min_level", output_remaining_min_level},
		{"ad_hoc_enc_ns", ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>::get_elapsed_time()}
	});
}


BENCHMARK(bm_parallel_spirit_with_eq_cmp)->Apply(args_spirit_with_eq_cmp)->MinTime(1e-12)->Repetitions(1);



#endif /* BENCHMARK_BM_SPIRIT_HPP_ */
