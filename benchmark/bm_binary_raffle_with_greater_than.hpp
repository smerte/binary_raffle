#ifndef BENCHMARK_BM_BINARY_RAFFLE_WITH_GREATER_THAN_HPP_
#define BENCHMARK_BM_BINARY_RAFFLE_WITH_GREATER_THAN_HPP_

#include <benchmark/benchmark.h>

#include "../ad_hoc_ctxt_iterator.hpp"
#include "../binary_raffle.hpp"
#include "../helib_context.hpp"
#include "../common.hpp"

static void args_binary_raffle_with_greater_than(benchmark::internal::Benchmark* b) {
	for (int input_bit_size = 3; input_bit_size <= 6; ++input_bit_size) {

		for (int log_value_bit_size = 6; log_value_bit_size <= 6; log_value_bit_size++) {
			b->Args({input_bit_size, 1, log_value_bit_size});
			b->Args({input_bit_size, 40, log_value_bit_size});
			b->Args({input_bit_size, 80, log_value_bit_size});
		}

//		size_t input_size = std::pow(2, input_bit_size);

//		b->Args({input_bit_size, 1});
//
//		int samples_count = ciel_log2(input_size);
//		b->Args({input_bit_size, samples_count});

//		samples_count = piecewise_binary_raffle::calc_default_samples_count(input_size);
		 //, samples_count});
	}
}





static void bm_piecewise_binary_raffle_with_greater_than_parallel(benchmark::State& state) {
	//initilize
	size_t single_thread_input_size = std::pow(2, state.range(0));
	size_t total_input_size = single_thread_input_size * get_hardware_thread_count();
//	size_t samples_count = piecewise_binary_raffle::calc_default_samples_count(single_thread_input_size);

	int bit_security = state.range(1);
	double failure_probability = 1L / (std::pow(2L, bit_security));
	size_t samples_count = piecewise_binary_raffle::calc_samples_count(single_thread_input_size, bit_security);
	size_t level = piecewise_binary_raffle::calc_level_approximation(samples_count) + 4;

	int log_value_bit_size = state.range(2);
	int value_bit_size = std::pow(2, log_value_bit_size);

	level += log_value_bit_size;

	helib_context context(false, level, 2, 1, 80, 3, 1, 128, 600);

	size_t thread_count = get_hardware_thread_count();


	//prepare input
	std::vector<std::vector<NewPlaintextArray>> plaintext(total_input_size, std::vector<NewPlaintextArray>(value_bit_size, NewPlaintextArray(*context.encrypted_array)));
	std::vector<long> plaintext_numbers(total_input_size, 0L);
//	plaintext.reserve(total_input_size);
	for (size_t i = 0; i < total_input_size; i++) {
		long current_value = 0;

		for (size_t j = 0; j < size_t(value_bit_size); j++) {

			long random_bit = rep(random_GF2());
			if (j > 30) {
				random_bit = 0;
			}

			current_value += random_bit * std::pow(2L, j);
			encode(*context.encrypted_array, plaintext[i][j], random_bit);
//
//			std::cout << "plaintext[" << i << "][" << j<< "] = " << plaintext[i][j] << endl;

		}

		plaintext_numbers[i] = current_value;

//		cout << "plaintext[" << i << "] = " << current_value << endl;
	}


	size_t actual_first_positive_index = 0;
	auto actual_first_positive_it = std::find_if(plaintext_numbers.begin(), plaintext_numbers.end(),  [] (auto x) {return x > 65536L && x < 1073741824L;});
	if (actual_first_positive_it != plaintext_numbers.end()) {
		actual_first_positive_index = std::distance(plaintext_numbers.begin(), actual_first_positive_it);
	}

	std::vector<NewPlaintextArray> lower_bound = std::vector<NewPlaintextArray>(value_bit_size, NewPlaintextArray(*context.encrypted_array));
	std::vector<NewPlaintextArray> upper_bound = std::vector<NewPlaintextArray>(value_bit_size, NewPlaintextArray(*context.encrypted_array));

	for (size_t j = 0; j < size_t(value_bit_size); j++) {
		if (j == 16) {
			encode(*context.encrypted_array, lower_bound[j], 1L);
		}
		else {
			encode(*context.encrypted_array, lower_bound[j], 0L);
		}

		if (j == 30) {
			encode(*context.encrypted_array, upper_bound[j], 1L);
		}
		else {
			encode(*context.encrypted_array, upper_bound[j], 0L);
		}
	}

	ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>
	lower_bound_it_begin(*context.public_key, *context.encrypted_array, lower_bound.begin());

	ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>
	upper_bound_it_begin(*context.public_key, *context.encrypted_array, upper_bound.begin());


	parallel_piecewise_secure_search_range_query br(*context.encrypted_array, *context.public_key, *context.secret_key,
		lower_bound_it_begin, upper_bound_it_begin, value_bit_size, samples_count);

//	//print input
//	for (size_t i = 0; i < total_input_size; i++) {
//		cout << i << "\t" << plaintext[i] << endl;
//	}

	//find actual first positive index
//	size_t actual_first_positive_index = 0;
//	NewPlaintextArray one_bit(*context.encrypted_array);
//	encode(*context.encrypted_array, one_bit, 1);
//	for (size_t i = 0; i < total_input_size; i++) {
//		if (equals(*context.encrypted_array, plaintext[i], one_bit)) {
//			actual_first_positive_index = i;
//			break;
//		}
//	}

	size_t found_first_positive_index = 0;
	bool all_ctxts_correct = true;
	for (auto _ : state) {
		state.PauseTiming();

		std::vector<ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>> enc_it_begin_vec;
		std::vector<ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>> enc_it_end_vec;
		for (size_t i = 0; i < plaintext.size(); i++) {
			enc_it_begin_vec.emplace_back(ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>(
				*context.public_key, *context.encrypted_array, plaintext[i].begin()));
			enc_it_end_vec.emplace_back(ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>(
				*context.public_key, *context.encrypted_array, plaintext[i].end()));
		}

		size_t single_thread_output_size = piecewise_binary_raffle::calc_output_size(single_thread_input_size);

		std::vector<std::vector<Ctxt>> output_vectors(thread_count, std::vector<Ctxt>(single_thread_output_size, Ctxt(*context.public_key)));

		std::vector<std::vector<Ctxt>::iterator> first_iterators;
		first_iterators.reserve(output_vectors.size());

		std::vector<std::vector<Ctxt>::iterator> last_iterators;
		last_iterators.reserve(output_vectors.size());

		for (size_t i = 0; i < output_vectors.size(); i++) {
			first_iterators.push_back(output_vectors[i].begin());
			last_iterators.push_back(output_vectors[i].end());
		}

		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>::reset_elapsed_time();
		state.ResumeTiming();
		br.process_piece(enc_it_begin_vec.begin(), enc_it_begin_vec.end());//, enc_it_end_vec.begin(), enc_it_end_vec.end());
		br.get_results(first_iterators, last_iterators);
		state.PauseTiming();

		for (size_t t = 0; t < thread_count; t++) {
			size_t local_found_first_positive_index = 0;
			for (size_t i = 0; i < output_vectors[t].size(); i++) {
				vector<long> ptxt;
				context.encrypted_array->decrypt(output_vectors[t][i], *context.secret_key, ptxt);
//				cout << i << " " << ptxt[0] << endl;
				local_found_first_positive_index += (ptxt[0] * std::pow(2, i));

				all_ctxts_correct = all_ctxts_correct && output_vectors[t][i].isCorrect();
			}

			if (found_first_positive_index == 0 && local_found_first_positive_index != 0) {
				found_first_positive_index = (t * single_thread_input_size) + local_found_first_positive_index - 1;
			}
		}
	}

	state.counters.insert({
		{"all_ctxts_correct", all_ctxts_correct},
		{"is_correct", (found_first_positive_index == actual_first_positive_index)},
		{"total_input_size", total_input_size},
		{"samples_count", samples_count},
		{"bit_security", bit_security},
		{"value_bit_size", value_bit_size},
		{"failure_probability", failure_probability},
		{"thread_count", thread_count},
		{"helib_level", level},
		{"helib_simd_size", context.encrypted_array->size()},
		{"helib_m", context.param_m},
		{"ad_hoc_enc_ns", ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>::get_elapsed_time()}
	});
}

BENCHMARK(bm_piecewise_binary_raffle_with_greater_than_parallel)->Apply(args_binary_raffle_with_greater_than)->MinTime(1e-12)->Repetitions(1);

#endif /* BENCHMARK_BM_BINARY_RAFFLE_WITH_GREATER_THAN_HPP_ */
