#ifndef AD_HOC_CTXT_ITERATOR_HPP_
#define AD_HOC_CTXT_ITERATOR_HPP_

#include <FHE/FHE.h>
#include <FHE/EncryptedArray.h>
#include <chrono>

template<class InputIt>
class ad_hoc_ctxt_iterator {
private:
	const FHEPubKey& _public_key;
	const EncryptedArray& _ea;
	InputIt _plaintext_iterator;


	static std::atomic_ullong _elapsed_time;
public:
	ad_hoc_ctxt_iterator(const FHEPubKey& public_key, const EncryptedArray& ea, InputIt plaintext_iterator) :
		_public_key(public_key), _ea(ea), _plaintext_iterator(plaintext_iterator)	{	}

	ad_hoc_ctxt_iterator(const ad_hoc_ctxt_iterator& other) :
		_public_key(other._public_key), _ea(other._ea), _plaintext_iterator(other._plaintext_iterator) {	}

	Ctxt operator*() const {
		std::chrono::high_resolution_clock::time_point begin = std::chrono::high_resolution_clock::now();

		Ctxt out(_public_key);
		_ea.encrypt(out, _public_key, *_plaintext_iterator);

		std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
		_elapsed_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count();
		return out;
	}

	std::shared_ptr<Ctxt> operator->() const {
		std::chrono::high_resolution_clock::time_point begin = std::chrono::high_resolution_clock::now();

		std::shared_ptr<Ctxt> out(new Ctxt(_public_key));
		_ea.encrypt(*out, _public_key, *_plaintext_iterator);

		std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
		_elapsed_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count();
		return out;
	}

	ad_hoc_ctxt_iterator& operator++() {
		++_plaintext_iterator;
		return *this;
    }

	ad_hoc_ctxt_iterator operator++(int) {
		return ad_hoc_ctxt_iterator(_public_key, _ea, _plaintext_iterator++);
	}

	ad_hoc_ctxt_iterator& operator--() {
		--_plaintext_iterator;
		return *this;
    }

	ad_hoc_ctxt_iterator operator--(int) {
		return ad_hoc_ctxt_iterator(_public_key, _ea, _plaintext_iterator--);
	}

	Ctxt operator[](typename std::iterator_traits<InputIt>::difference_type n) const {
		std::chrono::high_resolution_clock::time_point begin = std::chrono::high_resolution_clock::now();

		Ctxt out(_public_key);
		_ea.encrypt(out, _public_key, _plaintext_iterator[n]);

		std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
		_elapsed_time += std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count();
		return out;
	}

	ad_hoc_ctxt_iterator& operator+=(typename std::iterator_traits<InputIt>::difference_type n) {
		_plaintext_iterator += n;
		return *this;
	}

	ad_hoc_ctxt_iterator operator+(typename std::iterator_traits<InputIt>::difference_type n) const {
		return ad_hoc_ctxt_iterator(_public_key, _ea, _plaintext_iterator + n);
	}

	ad_hoc_ctxt_iterator& operator-=(typename std::iterator_traits<InputIt>::difference_type n) {
		_plaintext_iterator -= n;
		return *this;
	}

	ad_hoc_ctxt_iterator operator-(typename std::iterator_traits<InputIt>::difference_type n) const {
		return ad_hoc_ctxt_iterator(_public_key, _ea, _plaintext_iterator - n);
	}

	const InputIt& base() const {
		return _plaintext_iterator;
	}

	static unsigned long long int get_elapsed_time() {
		return _elapsed_time;
	}
	static void reset_elapsed_time() {
		_elapsed_time = 0;
	}
};

template<class InputIt>
std::atomic_ullong ad_hoc_ctxt_iterator<InputIt>::_elapsed_time = {0};

// Forward iterator requirements
template<class InputIt>
bool operator==(const ad_hoc_ctxt_iterator<InputIt>& left, const ad_hoc_ctxt_iterator<InputIt>& right) {
	return left.base() == right.base();
}

template<class InputIt>
bool operator!=(const ad_hoc_ctxt_iterator<InputIt>& left, const ad_hoc_ctxt_iterator<InputIt>& right) {
	return left.base() != right.base();
}

// Random access iterator requirements
template<class InputIt>
bool operator<(const ad_hoc_ctxt_iterator<InputIt>& left, const ad_hoc_ctxt_iterator<InputIt>& right) {
	return left.base() < right.base();
}

template<class InputIt>
bool operator>(const  ad_hoc_ctxt_iterator<InputIt>& left, const ad_hoc_ctxt_iterator<InputIt>& right) {
	return left.base() > right.base();
}

template<class InputIt>
bool operator<=(const ad_hoc_ctxt_iterator<InputIt>& left, const ad_hoc_ctxt_iterator<InputIt>& right) {
	return left.base() <= right.base();
}

template<class InputIt>
bool operator>=(const ad_hoc_ctxt_iterator<InputIt>& left, const ad_hoc_ctxt_iterator<InputIt>& right) {
	return left.base() >= right.base();
}

template <class InputIt>
struct std::iterator_traits<ad_hoc_ctxt_iterator<InputIt>>
{
    typedef typename InputIt::difference_type difference_type;
    typedef typename InputIt::value_type value_type;
    typedef typename InputIt::pointer pointer;
    typedef typename InputIt::reference reference;
    typedef std::input_iterator_tag iterator_category;
};


#endif /* AD_HOC_CTXT_ITERATOR_HPP_ */
