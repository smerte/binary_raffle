/*
 * spirit.hpp
 *
 *  Created on: May 15, 2018
 *      Author: crypto
 */

#ifndef SPIRIT_HPP_
#define SPIRIT_HPP_

#include <FHE/FHE.h>
#include "ctxt_heap.hpp"

#include <functional>
#include <algorithm>
#include <thread>

class eulers_phi_function {
public:
	static Ctxt is_zero(const Ctxt &num) {
		Ctxt result(ZeroCtxtLike, num);
		result.addConstant(ZZ(1));

		Ctxt is_non_zero_result = is_non_zero(num);
		result.addCtxt(is_non_zero_result, true);
		return result;
	}

	static Ctxt is_non_zero(const Ctxt &num) {
		unsigned long p = num.getContext().zMStar.getP();
		long int phi_p = phi_N(p);
		Ctxt result(ZeroCtxtLike, num);
		result = num;
		result.power(phi_p);
		return result;
	}

	static Ctxt equal(const Ctxt &num1, const Ctxt &num2) {
		Ctxt diff (ZeroCtxtLike, num1);
		diff = num1;
		diff.addCtxt(num2, true);
		Ctxt result = is_zero(diff);
		return result;
	}

	static Ctxt not_equal(const Ctxt &num1, const Ctxt &num2) {
		Ctxt diff (ZeroCtxtLike, num1);
		diff = num1;
		diff.addCtxt(num2, true);
		Ctxt result = is_non_zero(diff);
		return result;
	}
};

class spirit {
private:
	const EncryptedArray& _ea;
	const FHEPubKey& _public_key;
	const FHESecKey& _secret_key;
public:

	static size_t calc_output_size(size_t input_size) {
		return ciel_log2(input_size);
	}

	static long calc_level_approximation(size_t log_input_size, size_t prime) {
		long levels = 2 * ciel_log2(phi_N(prime)) + ciel_log2(log_input_size) + 3;
		return levels;
	}

	spirit(const EncryptedArray& ea, const FHEPubKey& public_key, const FHESecKey& secret_key) :
		_ea(ea),
		_public_key(public_key),
		_secret_key(secret_key)

	{	}

	template<class InputIt, class OutputIt>
	void execute(InputIt first, InputIt last, OutputIt output_first) {
		size_t input_size = std::distance(first, last);
		size_t output_size = ciel_log2(input_size);

		ctxt_heap_sum bins;
		std::vector<ctxt_heap_sum> output_bins = std::vector<ctxt_heap_sum>(output_size);

		std::vector<std::shared_ptr<Ctxt>> compare_cache(output_size, nullptr);

		InputIt i = first;
		bins.store(*i);

//		cout << "count = " << 0 << " input level = " << i->findBaseLevel() << endl;
		++i;
		size_t count = 1;
		while (i != last) {
			ctxt_heap_product mul_bin;

			if (!bins.is_empty(0)) {
				Ctxt tmp(ZeroCtxtLike, *first);
				tmp.addConstant(ZZ(1));
				tmp.addCtxt(bins.at(0), true);
				mul_bin.store(tmp);
			}

			for (size_t level = 1; level < output_size; ++level) {
				if (!bins.is_empty(level)) {
					if (compare_cache[level] == nullptr) {
						compare_cache[level] = std::shared_ptr<Ctxt>(new Ctxt(ZeroCtxtLike, *first));
						*compare_cache[level] = eulers_phi_function::is_zero(bins.at(level));
//						cout << "count = " << count << " compare_cache[" << level << "] level = " << compare_cache[level]->findBaseLevel() << endl;
					}

					mul_bin.store(*compare_cache[level]);
				}
				else {
					compare_cache[level] = nullptr;
				}
			}

			mul_bin.store(*i);

			Ctxt add = Ctxt(ZeroCtxtLike, *first);
			mul_bin.extract_result(add);
//			cout << "count = " << count << " mul_bin_result level = " << add.findBaseLevel() << endl;

			for (size_t bit = 0; bit < output_size; ++bit) {
				if (count & (1 << bit)) {
					output_bins[bit].store(add);
				}
			}

			bins.store(*i);

			++count;
			++i;
		}

		for (size_t i = 0; i < compare_cache.size(); ++i) {
			if (compare_cache[i] != nullptr) {
				compare_cache[i] = nullptr;
			}
		}

		for (size_t bit = 0; bit < output_size; ++bit) {
			if  (!output_bins[bit].is_empty()) {
				output_bins[bit].extract_result(*(output_first + bit));
				*(output_first + bit) = eulers_phi_function::is_non_zero(*(output_first + bit));
//				cout << "output bit[" << bit << "] level = " << (output_first + bit)->findBaseLevel() << endl;

			}
			else {
				*(output_first + bit) = Ctxt(ZeroCtxtLike, *first);
			}
		}
	}

	~spirit() {	}
};



class parallel_spirit {
private:
	uint _threads_count;
	std::vector<spirit> _spirits;
public:
	parallel_spirit(const EncryptedArray& ea, const FHEPubKey& public_key, const FHESecKey& secret_key, uint threads_count = 0) :
		_threads_count(threads_count) {
		if (_threads_count == 0) {
			_threads_count  = get_hardware_thread_count();
		}

		_spirits = std::vector<spirit>(_threads_count, spirit(ea, public_key, secret_key));
	}

	template<class InputIt, class OutputIt>
	void execute(InputIt first, InputIt last, std::vector<OutputIt> outputs_first) {
		size_t n = std::distance(first, last);
		size_t block_size = n / _threads_count;

		std::vector<std::thread> threads(_threads_count - 1);

		InputIt block_start = first;
		for(size_t t = 0; t < _threads_count - 1; t++) {
			InputIt block_end = block_start;
			std::advance(block_end, block_size);

			threads[t] = std::thread([this, t] (InputIt block_first, InputIt block_last, OutputIt output_first) {
				_spirits[t].execute(block_first, block_last, output_first);
			}, block_start, block_end, outputs_first[t]);

			std::advance(block_start, block_size);
		}

		_spirits[_threads_count - 1].execute(block_start, last, outputs_first[_threads_count - 1]);

		std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
	}
};


template<class InputIt>
void compare_with_squaring(Ctxt& output, const InputIt left, const InputIt right, size_t size) {
//	assert(left.size() == right.size());
	ctxt_heap_product product_heap;

	for (size_t i = 0; i < size; i++) {
		Ctxt tmp(ZeroCtxtLike, output);
		tmp.addConstant(ZZ(1));

		Ctxt tmp_sqr(ZeroCtxtLike, output);
		tmp_sqr.addCtxt(*(left + i));
		tmp_sqr.addCtxt(*(right + i), /*negative=*/ true);
		tmp_sqr.square();

		tmp.addCtxt(tmp_sqr, /*negative=*/ true);
		product_heap.store(tmp);
	}

	product_heap.extract_result(output);
}

template<class QueryInputIt>
class parallel_piecewise_spirit_secure_search {
private:
	uint _threads_count;
	std::vector<spirit> _spirits;
	QueryInputIt _query_first;
	size_t _query_size;
public:
	parallel_piecewise_spirit_secure_search(const EncryptedArray& ea, const FHEPubKey& public_key, const FHESecKey& secret_key,
		QueryInputIt query_first, size_t query_size, uint threads_count = 0) :
		_threads_count(threads_count), _query_first(query_first), _query_size(query_size) {
		if (_threads_count == 0) {
			_threads_count  = get_hardware_thread_count();
		}

		_spirits = std::vector<spirit>(_threads_count, spirit(ea, public_key, secret_key));
	}


	template<class InputIt, class OutputIt>
	void execute(InputIt first, InputIt last, std::vector<OutputIt> outputs_first) {
		size_t n = std::distance(first, last);
		size_t block_size = n / _threads_count;

		std::vector<std::thread> threads(_threads_count - 1);

		InputIt block_start = first;
		for(size_t t = 0; t < _threads_count - 1; t++) {
			InputIt block_end = block_start;
			std::advance(block_end, block_size);

			threads[t] = std::thread([this, t] (InputIt block_first, InputIt block_last, OutputIt output_first) {
				std::vector<Ctxt> cmp_result(std::distance(block_first, block_last), Ctxt(ZeroCtxtLike, *(block_first[0])));
				InputIt current = block_first;
				for (size_t i = 0; current != block_last; i++) {
					compare_with_squaring(cmp_result[i], *current, this->_query_first, this->_query_size);
					++current;
				}

				_spirits[t].execute(cmp_result.begin(), cmp_result.end(), output_first);
			}, block_start, block_end, outputs_first[t]);

			std::advance(block_start, block_size);
		}

		std::vector<Ctxt> cmp_result(std::distance(block_start, last), Ctxt(ZeroCtxtLike, *(block_start[0])));
		InputIt current = block_start;

		for (size_t i = 0; current != last; i++) {
			compare_with_squaring(cmp_result[i], *current, this->_query_first, this->_query_size);
			++current;
		}

		_spirits[_threads_count - 1].execute(cmp_result.begin(), cmp_result.end(), outputs_first[_threads_count - 1]);

		std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
	}
};


#endif /* SPIRIT_HPP_ */
