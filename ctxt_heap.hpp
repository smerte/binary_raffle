#ifndef INCLUDE_BINOMIAL_TOURNAMENT_HPP_
#define INCLUDE_BINOMIAL_TOURNAMENT_HPP_

#include <vector>
#include <memory>
#include <FHE/Ctxt.h>

class ctxt_heap_sum {
private:
	std::vector<std::shared_ptr<Ctxt>> _heap;

public:
	ctxt_heap_sum()  { }
	~ctxt_heap_sum() {
		clear();
	}

	void store(const Ctxt &ctxt, size_t level = 0) {
		std::shared_ptr<Ctxt> new_ctxt = std::shared_ptr<Ctxt>(new Ctxt(ZeroCtxtLike, ctxt));
		*new_ctxt = ctxt;

		bool inserted = false;

		while (!inserted) {
			if (_heap.size() <= level) {
				_heap.resize(level + 1);
			}

			if (!_heap[level]) {
				_heap[level] = new_ctxt;
				inserted = true;
			}
			else {
				new_ctxt->addCtxt(*_heap[level]);
				_heap[level] = nullptr;
				++level;
			}
		}
	}

	void extract_result(Ctxt& result) {
		result = Ctxt(ZeroCtxtLike, result);

		for (size_t i = 0; i < _heap.size(); ++i) {
			if (_heap[i] != nullptr) {
				result.addCtxt(*_heap[i]);
			}
		}
	}

	void clear() {
		for (size_t i = 0; i < _heap.size(); ++i) {
			_heap[i] = nullptr;
		}

		_heap.clear();
		_heap.shrink_to_fit();
	}

	size_t size() const {
		return _heap.size();
	}

	size_t levels() const {
		size_t levels = 0;
		for (size_t i = 0; i < _heap.size(); ++i) {
			if (_heap[i] != nullptr) {
				++levels;
			}
		}

		return levels;
	}

	Ctxt& at(size_t level) { return *_heap[level]; }

	bool is_empty() const { return levels() == 0; }

	bool is_empty(size_t level) const { return (level >= _heap.size()) || (_heap[level] == nullptr) ; }

};


class ctxt_heap_product {
private:
	std::vector<std::shared_ptr<Ctxt>> _heap;

public:
	ctxt_heap_product()  { }
	~ctxt_heap_product() {
		clear();
	}

	void store(const Ctxt &ctxt, size_t level = 0) {
		std::shared_ptr<Ctxt> new_ctxt = std::shared_ptr<Ctxt>(new Ctxt(ZeroCtxtLike, ctxt));
		*new_ctxt = ctxt;

		bool inserted = false;

		while (!inserted) {
			if (_heap.size() <= level) {
				_heap.resize(level + 1);
			}

			if (!_heap[level]) {
				_heap[level] = new_ctxt;
				inserted = true;
			}
			else {
				new_ctxt->multiplyBy(*_heap[level]);
				_heap[level] = nullptr;
				++level;
			}
		}
	}

	void extract_result(Ctxt& result) {
		std::vector<Ctxt> heap_content;

		for (size_t i = 0; i < _heap.size(); ++i) {
			if (_heap[i] != nullptr) {
				heap_content.push_back(*_heap[i]);
			}
		}

		totalProduct(result, heap_content);
	}

	void clear() {
		for (size_t i = 0; i < _heap.size(); ++i) {
			_heap[i] = nullptr;
		}

		_heap.clear();
		_heap.shrink_to_fit();
	}

	size_t size() const {
		return _heap.size();
	}

	size_t levels() const {
		size_t levels = 0;
		for (size_t i = 0; i < _heap.size(); ++i) {
			if (_heap[i] != nullptr) {
				++levels;
			}
		}

		return levels;
	}


	Ctxt& at(size_t level) { return *_heap[level]; }

	bool is_empty() const { return levels() == 0; }

	bool is_empty(size_t level) const { return (level >= _heap.size()) || (_heap[level] == nullptr) ; }

};

#endif /* INCLUDE_BINOMIAL_TOURNAMENT_HPP_ */
