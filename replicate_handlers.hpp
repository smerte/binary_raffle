#ifndef REPLICATE_HANDLERS_HPP_
#define REPLICATE_HANDLERS_HPP_

#include <FHE/FHE.h>
#include <FHE/EncryptedArray.h>
#include <FHE/replicate.h>

#include "ctxt_heap.hpp"

class stop_replicate_and_multiply { };


class replicate_and_multiply : public ReplicateHandler {
private:
	const EncryptedArray& _ea;

	ctxt_heap_product _ctxt_heap_product;
	size_t _handled_slots;
	size_t _max_slot;
	size_t _min_slot;


public:
	replicate_and_multiply(const EncryptedArray& ea, long max_slot = -1, size_t min_slot = 0) :
		_ea(ea),  _handled_slots(0), _min_slot(min_slot) {
		if (max_slot == -1) {
			max_slot = ea.size();
		}

		_max_slot = max_slot;
	}

	virtual void handle(const Ctxt& ctxt) {
		if (_handled_slots >= _min_slot) {
//			CheckCtxt(ctxt, ("handle " + std::to_string(_handled_slots)).c_str());
			_ctxt_heap_product.store(ctxt);
			_handled_slots++;
		}

		if (_handled_slots >= _max_slot) {
			throw stop_replicate_and_multiply();
		}
	}

//	virtual bool earlyStop(long d, long k, long prodDim) { return false; }


	void extract_result(Ctxt& result) {
		_ctxt_heap_product.extract_result(result);
	}
};


class replicate_and_update_result_bits : public ReplicateHandler {
private:
	const EncryptedArray& _ea;


	std::vector<Ctxt>& _result_bits;
	size_t _input_index;
	size_t _slot_index;
public:
	replicate_and_update_result_bits(const EncryptedArray& ea, std::vector<Ctxt>& result_bits, size_t input_index) :
		_ea(ea), _result_bits(result_bits), _input_index(input_index), _slot_index(0) {	}

	virtual void handle(const Ctxt& ctxt) {
		for (size_t b = 0; b < _result_bits.size(); b++) {
			size_t mask = 1 << b;
			size_t index = (_input_index * _ea.size()) + _slot_index + 1;
			size_t masked_index = index & mask;	//one based index, zero is reserved to empty input
			size_t bit_b_of_index = masked_index >> b;

			if (bit_b_of_index) {
				_result_bits[b].addCtxt(ctxt);
			}
		}

		_slot_index++;
	}
};



//Assumes that p=2 and r=1 and thus both 'left' and 'right' contain binary values in each slot.
void binary_equals(const EncryptedArray& ea, Ctxt& result, const Ctxt& left, const Ctxt& right, long slots = -1,  long rec_bound = 64) {
	Ctxt diff = left;
	diff += right;
	diff.addConstant(ZZ(1));

	replicate_and_multiply rep_and_mult(ea, slots);
	try {
	  replicateAll(ea, diff, &rep_and_mult, rec_bound);
	}
	catch (const stop_replicate_and_multiply& ex) { /* NO-OP */	}

	return rep_and_mult.extract_result(result);
}


#endif /* REPLICATE_HANDLERS_HPP_ */
