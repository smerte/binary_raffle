#ifndef TOEPLITZ_UNIVERSAL_HASH_HPP_
#define TOEPLITZ_UNIVERSAL_HASH_HPP_

#include <NTL/vec_GF2.h>

#include <boost/archive/iterators/ostream_iterator.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>

//#include <boost/beast/core/detail/base64.hpp>

class toeplitz_universal_hash {
private:
	//row length
	size_t _input_bit_length;

	//column length
	size_t _output_bit_length;

	size_t _max_records;

	vec_GF2 _matrix_A_first_row;
	vec_GF2 _matrix_A_first_column;

	vec_GF2 _vector_b;


	//Get some matrix row in whole or partially

	void get_row(vec_GF2& output, size_t row, size_t limit = 0) {
		if (limit == 0) {
			limit = _input_bit_length;
		}

		if (output.length() < long(limit)) {
			output.SetLength(limit);
		}

		clear(output);

		for (size_t column = 0; column < size_t(limit); column++) {
			if (row > column) {
				output[column] = _matrix_A_first_column[row - column];
			}
			else {
				output[column] = _matrix_A_first_row[column - row];
			}
		}
	}

	void get_row_part(vec_GF2& output, size_t row, size_t begin, size_t end) {
		size_t part_size = end - begin;

		if (output.length() < long(part_size)) {
			output.SetLength(part_size);
		}

		clear(output);

		size_t output_i = 0;
		for (size_t column = begin; column < end; column++) {
			if (row > column) {
				output[output_i] = _matrix_A_first_column[row - column];
			}
			else {
				output[output_i] = _matrix_A_first_row[column - row];
			}

			output_i++;
		}
	}

	//Base64 I/O helper functions

	static void vec_GF2_to_byte_vector(std::vector<unsigned char>& output, const vec_GF2& input){
		size_t output_size = std::ceil(double(input.length()) / 8.0);
		output.clear();
		output.resize(output_size);

		for (size_t i = 0; i < output_size; i++) {
			unsigned char byte = 0;
			for (unsigned short j = 0; j <= 8; j++) {
				if ((i * 8) + j < size_t(input.length())) {
					if (rep(input[(i * 8) + j]) == 1) {
						byte |= 1 << j;
					}
				}
			}

			output[i] = byte;
		}
	}

	static void byte_vector_to_vec_GF2(vec_GF2& output, const std::string& input, size_t output_size) {
		output.SetLength(output_size);
		clear(output);

		for (size_t i = 0; i < input.size(); i++) {
			unsigned char byte = input[i], bit;
			for (unsigned short j = 0; j <= 8; j++) {
				if ((i * 8) + j < output_size) {
					bit = (byte >> j) & 0x1;
					output[(i * 8) + j] = GF2(bit);
				}
			}
		}
	}

	static void write_vec_GF2_as_base64(ostream& out, const vec_GF2& input) {
		typedef boost::archive::iterators::base64_from_binary<
							boost::archive::iterators::transform_width<
								std::vector<unsigned char>::iterator, 6, 8
							>
						> to_base64_text;

		std::vector<unsigned char> tmp;
		vec_GF2_to_byte_vector(tmp, input);

		std::copy(
			to_base64_text(tmp.begin()),
			to_base64_text(tmp.end()),
			ostream_iterator<unsigned char>(out)
		);
	}

	static void read_base64_to_vec_GF2(istream& in, vec_GF2& output, size_t output_size) {
		typedef boost::archive::iterators::transform_width<
					boost::archive::iterators::binary_from_base64<
						string::const_iterator
					>, 8, 6
				> from_base64_text;

		std::string str_tmp;
		in >> str_tmp;

		std::stringstream ss;

		std::copy(
			from_base64_text(str_tmp.begin()),
			from_base64_text(str_tmp.end()),
			ostream_iterator<unsigned char>(ss)
		);


		byte_vector_to_vec_GF2(output, ss.str(), output_size);
	}


public:

	toeplitz_universal_hash() :	_input_bit_length(0), _output_bit_length(0), _max_records(0) {}

	toeplitz_universal_hash(size_t input_bit_length, size_t max_records, size_t output_bit_length = 0) :
		_input_bit_length(input_bit_length), _max_records(max_records) {

		if (output_bit_length == 0) {
			_output_bit_length = 3 * std::ceil(std::log2(_max_records));
		}
	}

	void init_random() {
		_matrix_A_first_row = random_vec_GF2(_input_bit_length);
		_matrix_A_first_column = random_vec_GF2(_output_bit_length);
		_matrix_A_first_column[0] = _matrix_A_first_row[0];
		_vector_b = random_vec_GF2(_output_bit_length);
	}

	//Getters

	size_t get_input_bit_length() const { return _input_bit_length;	}

	size_t get_output_bit_length() const { return _output_bit_length; }

	size_t get_max_records() const { return _max_records; }


	//Transform the data in chunks using 3 steps: begin, step, end. Each step can have different chunk size

	void transform_begin(vec_GF2& digest) {
		digest.SetLength(_output_bit_length);
		clear(digest);
	}

	size_t transform_step(vec_GF2& digest, const vec_GF2& data_chunk, size_t begin_index) {
		size_t current_size = data_chunk.length();
		size_t end_index = begin_index + current_size;

		if (begin_index >= _input_bit_length) {
			return -1;
		}

		if (end_index > _input_bit_length) {
			end_index = _input_bit_length;
		}

		for (size_t row_i = 0; row_i < _output_bit_length; row_i++) {
			GF2 current_inner_product;
			vec_GF2 current_row_part;

			get_row_part(current_row_part, row_i, begin_index, end_index);
			InnerProduct(current_inner_product, current_row_part, data_chunk);
			digest[row_i] += current_inner_product;
		}

		return end_index;
	}

	void transform_end(vec_GF2& digest) {
		digest += _vector_b;
	}

	//Transform data iteratively, each iteration will process 'buffer_size' bits

	void transform_buffered(vec_GF2& digest, const vec_GF2& data, size_t buffer_size = 100000) {

		digest.SetLength(_output_bit_length);
		clear(digest);

		size_t end = data.length();
		if (end > _input_bit_length) {
			end = _input_bit_length;
		}


		for (size_t row_i = 0; row_i < _output_bit_length; row_i++) {
			size_t current_begin = 0;
			GF2 total_inner_product(0);

			while (current_begin < end) {
				GF2 current_inner_product;
				vec_GF2 current_row_part, current_data_part;
				size_t current_end = current_begin + buffer_size;
				if (current_end > end) {
					current_end = end;
				}

				size_t current_size = current_end - current_begin;
				current_data_part.SetLength(current_size);
				for (size_t d = 0; d < current_size; d++) {
					current_data_part[d] = data[current_begin + d];
				}

				get_row_part(current_row_part, row_i, current_begin, current_end);
				InnerProduct(current_inner_product, current_row_part, current_data_part);
				total_inner_product += current_inner_product;

				current_begin = current_end;
			}

			digest[row_i] += total_inner_product;
		}

		digest += _vector_b;
	}


	//Transform data as whole

	void transform(vec_GF2& digest, const vec_GF2& data) {
		digest.SetLength(_output_bit_length);
		clear(digest);

		size_t data_length = data.length();
		if (data_length > _input_bit_length) {
			data_length = _input_bit_length;
		}

		vec_GF2 current_row;
		for (size_t i = 0; i < _output_bit_length; i++) {
			get_row(current_row, i, data_length);
			InnerProduct(digest[i], current_row, data);
		}

		digest += _vector_b;
	}

	bool operator==(const toeplitz_universal_hash& other) const {
		if (_input_bit_length != other._input_bit_length ||
			_output_bit_length != other._output_bit_length ||
			_max_records != other._max_records ||
			_matrix_A_first_row != other._matrix_A_first_row ||
			_matrix_A_first_column != other._matrix_A_first_column ||
			_vector_b != other._vector_b) {
			return false;
		}

		return true;
	}


	//Base64 I/O methods

	friend istream& operator>>(istream& in, toeplitz_universal_hash& hash){
		in >> hash._input_bit_length;
		in >> hash._output_bit_length;
		in >> hash._max_records;

		read_base64_to_vec_GF2(in, hash._matrix_A_first_row, hash._input_bit_length);
		read_base64_to_vec_GF2(in, hash._matrix_A_first_column, hash._output_bit_length);
		read_base64_to_vec_GF2(in, hash._vector_b, hash._output_bit_length);
//		boost::beast::detail::base64_decode();

		return in;
	}

	friend ostream& operator<<(ostream& out, const toeplitz_universal_hash& hash) {


		out << hash._input_bit_length << endl;
		out << hash._output_bit_length << endl;
		out << hash._max_records << endl;

		write_vec_GF2_as_base64(out, hash._matrix_A_first_row);
		out << endl;

		write_vec_GF2_as_base64(out, hash._matrix_A_first_column);
		out << endl;

		write_vec_GF2_as_base64(out, hash._vector_b);
		out << endl;

		return out;
	}
};

#endif /* TOEPLITZ_UNIVERSAL_HASH_HPP_ */

