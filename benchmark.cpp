//#include <NTL/BasicThreadPool.h>
#include <benchmark/benchmark.h>
#include "common.hpp"

//#include "benchmark/bm_spirit.hpp"
//#include "benchmark/bm_binary_raffle.hpp"
#include "benchmark/bm_binary_raffle_with_eq_cmp.hpp"
//#include "benchmark/bm_spirit_with_eq_cmp.hpp"
//#include  "benchmark/bm_binary_raffle_with_greater_than.hpp"

#include <cstring>
#include <cstdlib>
#include <stdio.h>
#include <time.h>

int main(int argc, char** argv) {

//	 NTL::SetNumThreads(get_hardware_thread_count());

//	 if (argc == 1) {
//		 //put default arguments
//		 const int arguments_size = 4;
//		 char* arguments[arguments_size];
//		 for (int i = 0; i < arguments_size; i++) {
//			 arguments[i] = (char*)std::malloc(sizeof(char) * 256);
//		 }
//
//		 std::strncpy(arguments[0], argv[0], 256);
//
//		 std::strcpy(arguments[1], "--benchmark_format=json");
//		 std::strcpy(arguments[2], "--benchmark_out_format=json");
//
//		 time_t rawtime;
//		 struct tm * timeinfo;
//		 time (&rawtime);
//		 timeinfo = localtime (&rawtime);
//		 strftime (arguments[3], 256, "--benchmark_out=output_%Y_%m_%d_%H_%M_%S.json", timeinfo);
//
//		 int arguments_count = arguments_size;
//
//		 //run benchmarks
//		 ::benchmark::Initialize(&arguments_count, arguments);
//		 if (::benchmark::ReportUnrecognizedArguments(arguments_count, arguments)) return 1;
//		 ::benchmark::RunSpecifiedBenchmarks();
//
////		 //free allocated memory
////		 for (int i = 0; i < arguments_size; i++) {
////			 std::free(arguments[i]);
////		 }
//	 }
//	 else {
		 //run benchmarks with command line given arguments
		 ::benchmark::Initialize(&argc, argv);
		 if (::benchmark::ReportUnrecognizedArguments(argc, argv)) return 1;
		 ::benchmark::RunSpecifiedBenchmarks();
//	 }



}
