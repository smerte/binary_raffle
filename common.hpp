#ifndef COMMON_HPP_
#define COMMON_HPP_


#include <cmath>
size_t ciel_log2(size_t x) {
	size_t log2_x = std::log2(x);
	return (x - (1 << log2_x)) ? (log2_x + 1) : log2_x;
}

#include <thread>
size_t get_hardware_thread_count() {
//	const size_t default_hardware_threads = 16;
//	size_t hardware_threads = std::thread::hardware_concurrency();
//	return hardware_threads != 0 ? hardware_threads : default_hardware_threads;
	return 1;
}

#include <iostream>
#include <FHE/FHE.h>
#include <FHE/EncryptedArray.h>

void decrypt_and_print(ostream& s, const Ctxt& ctxt, const FHESecKey& sk, const EncryptedArray& ea) {
  vector<ZZX> ptxt;
  ea.decrypt(ctxt, sk, ptxt);
  printZZX(s, ptxt[0]);

}

#endif /* COMMON_HPP_ */
