# Description
This is the official repository for the implementions of the following paper:
[Akavia, A., Gentry, C., Halevi, S., & Leibovich, M. (2019). Setup-Free Secure Search on Encrypted Data: Faster and Post-Processing Free. Proceedings on Privacy Enhancing Technologies, 3, 87-107.](https://petsymposium.org/2019/files/papers/issue3/popets-2019-0038.pdf "Akavia, A., Gentry, C., Halevi, S., & Leibovich, M. (2019). Setup-Free Secure Search on Encrypted Data: Faster and Post-Processing Free. Proceedings on Privacy Enhancing Technologies, 3, 87-107.")


The source code implements [Binary Raffle](http://www.cs.mta.ac.il/staff/Adi_Akavia/pdf/Maxim%20Leibovich.pdf "Binary Raffle") protocol using [HELib](https://github.com/shaih/HElib/ "HELib") (version from December 2018). Notice that the [current versions](https://github.com/homenc/HElib "current versions") of HELib are not supported. 

The implementaion is in C++17, compiled with CMake and tested on  Ubuntu Server 16.04.4.